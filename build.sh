!/bin/sh

NODE_PROJECTS_ROOT=packages
TALEND_PROJECTS_ROOT=talend

build_node_importers() {
    for PROJ in $(find $1 -maxdepth 1 -mindepth 1 -type d); do
        PROJ_NAME=$(basename $PROJ)
        echo "Building importer $PROJ_NAME"
        cp -R $PROJ dist
        echo "Copy $PROJ to dist"
        OLD_PWD=$PWD
        cd "dist/$PROJ_NAME"
        npm install
        echo "after npm install"
        npm run build
        echo "after run build"
        cd "$OLD_PWD"
    done
}

build_talend_importers() {
    for IMPORTER in $(find $1/bin -maxdepth 1 -type f -name '*.zip'); do
        PROC=$(basename $IMPORTER .zip)
        echo "Building Talend importer $PROC"
        MANIFEST="$1/bin/${PROC}_manifest.json"
        DEST_DIR="dist/$PROC"
        mkdir "$DEST_DIR"
        unzip $IMPORTER -d $DEST_DIR
        cp "$MANIFEST" "$DEST_DIR/manifest.json"
        echo "after copy $MANIFEST"

    done
}

#rm -rf dist
#mkdir dist
build_node_importers "$NODE_PROJECTS_ROOT"
build_talend_importers "$TALEND_PROJECTS_ROOT"
