const { ReadStream } = require('fs');
const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const { v1: uuidV1 } = require('uuid');
const { read } = require('xlsx');
const { FILE_STRUCTURE } = require('./definitions/file-structure');
const { normalize } = require('./normalize');
const { getValueOrDefault, incCol } = require('./sheet-utils');
const { streamToBuffer } = require('./stream-to-buffer');
const { date1900 } = require('./utils');
const { winston } = require('./winston');

module.exports = {
    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {Record<string, unknown>} options
     */
    async transform(src, dest, options) {
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            winston.info(`Reading source file ${src.path}…`);
        } else {
            winston.info('Reading source stream…');
        }

        let book = await prepareWorkbook(src);
        winston.info(`Preparing input data from Excel workbook…`);
        let data = book.SheetNames
            .filter(name => /^(?:\d+|RI.)$/.test(name))
            .reduce(
                addSheetToTableFrom(book),
                getEmptyTable()
            );
        book = null;

        const normalized = normalize(data);
        data = null;
        return output(normalized, dest, transformation, options);
    }
};

/**
 *
 * @param {Readable} src
 */
async function prepareWorkbook(src) {
    const srcBuf = await streamToBuffer(src);

    return read(srcBuf);
}

/**
 *
 * @param {import('xlsx/types').WorkBook} book
 * @returns {(table: any[][], sheetName: string) => any[][]}
 */
function addSheetToTableFrom(book) {
    return function (table, sheetName) {
        const sheet = book.Sheets[sheetName];
        const sheetData = transformSheet(sheetName, sheet);
        table.push(...sheetData);

        return table;
    }
}

/**
 *
 * @param {string} sheetName
 * @param {import('xlsx/types').WorkSheet} sheet
 * @returns {any[][]}
 */
function transformSheet(sheetName, sheet) {
    const sheetData = [];

    const names = [sheetName];
    const initCol = findStartCol(sheet);

    for (let col = initCol; sheet[`${col}9`] && sheet[`${col}9`].v; col = incCol(col)) {
        const row = readMonth(sheet, col, names);
        sheetData.push(row);
    }

    return sheetData;
}

function findStartCol(sheet, currentCol = 'B', minDate1900 = date1900('2009-01-01')) {
    if (!sheet[`${currentCol}9`] || !sheet[`${currentCol}9`].v || sheet[`${currentCol}9`].v >= minDate1900) {
        return currentCol;
    }

    return findStartCol(sheet, incCol(currentCol), minDate1900);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} col
 * @param {ReadonlyArray<string>} names
 * @returns {any[]}
 */
function readMonth(sheet, col, names) {
    /**
     * @type any[]
     */
    const general = [
        uuidV1(),
        ...names
    ];

    return FILE_STRUCTURE.reduce((dataRow, field) => (dataRow.push(getNumber(sheet, `${col}${field.row}`)), dataRow), general);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} address
 * @returns {number | null}
 */
function getNumber(sheet, address) {
    const raw = getValueOrDefault(sheet, address, null);
    const numeric = +raw;

    return isNaN(numeric) || raw === undefined || raw === null || typeof raw === 'string' && !raw.trim()
        ? null
        : numeric;
}

/**
 * @returns {any[][]}
 */
function getEmptyTable() {
    return [[
        'id',
        'areaId',
        ...FILE_STRUCTURE.map(field => field.property)
    ]];
}

/**
 *
 * @param {import('./normalize').NormalizedData} data
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function output(data, dest, transformation, options) {
    await new Promise((resolve, reject) => {
        const outData = loadTransformation(transformation).default(data, options);

        if (isStream.readable(outData)) {
            outData.pipe(dest);
            dest.on('finish', () => resolve());
            outData.on('error', reject);
        } else {
            dest.write(
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Failed to load transformation ${transformation}`);
    }
}
