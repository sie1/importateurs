const { identity, invert, stubTrue } = require('lodash/fp');
const moment = require('moment');
const { v1: uuidV1 } = require('uuid');
const { NORMALIZATIONS } = require('./definitions/normalization');
const { winston } = require('./winston');

/**
 * @typedef {Object} NormalizedData
 * @property {string} importId
 * @property {(string[] | [string, string, string, string, string, string, string, number])[]} rows
 */

const JIRAMA_COMPANY_UUID = '11556ea8-3b66-11e9-ae4e-0242113dc113';

/**
 *
 * @param {any[][]} table
 * @returns {NormalizedData}
 */
function normalize(table) {
    winston.info('Normalizing data…');

    const rows = [['id', 'mois', 'id_societe', 'centrale', 'timestamp', 'import', 'code', 'valeur']];
    const importId = uuidV1();
    const normalized = table.slice(1).reduce(addTableRowToNormalizedMatrix(importId, table[0]), rows);

    return { importId, rows: normalized };
}

function addTableRowToNormalizedMatrix(importId, colNames) {
    const colIds = invert(colNames);
    const timestamp = moment().format();

    return function(normRows, row) {
        function get(col, mod) {
            if (Array.isArray(col)) {
                return (mod || identity)(col.reduce((acc, subCol) => acc + get(subCol), 0));
            }

            const raw = row[colIds[col]];
            const normalized = typeof raw === 'undefined'
                ? undefined
                : raw === null || raw === ''
                ? null
                : !isNaN(+raw)
                ? +raw
                : typeof raw === 'string'
                ? raw
                : null;

            return (mod || identity)(normalized);
        }

        const rowBaseDimensions = [
            get('month', formatDate),
            JIRAMA_COMPANY_UUID,
            get('areaId'),
            timestamp,
            importId
        ];

        const addNormRow = normRowBuilderFactory(rowBaseDimensions);
        NORMALIZATIONS
            .filter(normalization => (normalization.conditional || stubTrue)(rowBaseDimensions))
            .forEach(normalization => addNormRow(normRows, normalization.code, get(normalization.field, normalization.modification)));

        return normRows;
    };
}

/**
 *
 * @param {string | Date | number} input
 */
function formatDate(input) {
    return isDate1900(input)
        ? formatDate1900(input)
        : input
        ? moment(input).format('YYYY-MM-DD')
        : null;
}

const DATE1900_2009_01_01 = 38716;
const DATE1900_FAR_FUTURE = 60000;

function isDate1900(value) {
    return typeof value === 'number' && value >= DATE1900_2009_01_01 && value < DATE1900_FAR_FUTURE;
}

function formatDate1900(date1900) {
    return moment('1899-12-31').add(date1900, 'days').date(1).format('YYYY-MM-DD');
}

function normRowBuilderFactory(baseDimensions) {
    return function addCopyRow(rows, code, value) {
        if (value === null || value === undefined) {
            return;
        }

        rows.push([uuidV1(), ...baseDimensions, code, value]);
    }
}

module.exports = {
    normalize
};
