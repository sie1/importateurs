module.exports = {
    async streamToBuffer(stream) {
        return new Promise((resolve, reject) => {
            const parts = [];
            stream.on('data', buf => parts.push(buf));
            stream.on('end', () => {
                resolve(Buffer.concat(parts));
            })
            stream.on('error', reject);
        });
    }
};
