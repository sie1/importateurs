const xlsx = require('xlsx');

module.exports = {
    default(tables) {
        const wb = xlsx.utils.book_new();
        const ws = xlsx.utils.aoa_to_sheet(tables);
        xlsx.utils.book_append_sheet(wb, ws, 'EVOLME');
        return xlsx.write(wb, { type: 'buffer', compression: true });
    }
}
