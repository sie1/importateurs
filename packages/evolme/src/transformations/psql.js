const { basename } = require('path');
const { Readable } = require('stream');
const { winston } = require('../winston');

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, options) {

        return new PsqlScriptStream(data, options);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, options) {
        super();

        this.filename = basename(options._[0]);
        this.rows= data.rows;
        this.importId = data.importId;
        this.options = options;
        this.iter = this._readLine();
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {
        yield 'begin transaction;\n';
        yield 'delete from etl_matrice m using imports i where m.import = i.id and i.type = \'EVOLME\';\n';
        yield `insert into imports(id, type, "timestamp", filename) values ('${this.importId}', 'EVOLME', NOW(), '${this.filename}');\n`;
        yield `copy etl_matrice(${this.rows[0].join(', ')}) from STDIN;\n`;

        winston.info('Converting data into PSQL copy data…');

        for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
            const rowData = this.rows[rowIndex];
            this.rows[rowIndex] = null;
            yield rowData.join('\t') + '\n';
        }

        yield '\\.\n\n';
        yield 'commit transaction;\n';
    }
}
