/** Multiplication par un facteur. Par ex. pour convertir les MW en kW utiliset `fac(1000)`,
 * ou pour l'inverse `fac(0.001)`. */
function fac(factor) {
    return function(val) {
        return val * factor;
    };
}

/** Appliquer un valeur par défaut où le valeur existant ne peut pas être automatiquement converti
 * en forme numérique. Par ex., `def(0)` va appliquer le valeur `0` oùun valeur est manquand. */
function def(defVal) {
    return function(val) {
        return isNaN(+val) ? defVal : val;
    }
}

module.exports = {
    def,
    fac
};
