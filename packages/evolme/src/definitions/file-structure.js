/**
 * @typedef {Object} DataField
 * @property {number} row
 * @property {string} property
 */


 /**
  * @type {DataField[]}
  */
 const FILE_STRUCTURE = [
    { row: 9, property: 'month' },
    { row: 10, property: 'grossProduction' },
    { row: 11, property: 'hydro' },
    { row: 15, property: 'thermalGO' },
    { row: 19, property: 'thermalFO' },
    { row: 23, property: 'thermalSolar' },
    { row: 26, property: 'hybrid' },
    { row: 27, property: 'netProduction' },
    { row: 28, property: 'energyDelivery' },
    { row: 32, property: 'consumptionGO' },
    { row: 36, property: 'consumptionFO' },
    { row: 44, property: 'piHydro' },
    { row: 48, property: 'piThermalGO' },
    { row: 52, property: 'piThermalFO' },
    { row: 56, property: 'piThermalSolar' },
    { row: 59, property: 'piHybrid' },
    { row: 61, property: 'pdHydro' },
    { row: 65, property: 'pdThermalGO' },
    { row: 69, property: 'pdThermalFO' },
    { row: 73, property: 'pdThermalSolar' },
    { row: 76, property: 'pdHybrid' },
    { row: 78, property: 'maxProduction' },
    { row: 84, property: 'saleMTPrivate' },
    { row: 85, property: 'saleMTAdministration' },
    { row: 86, property: 'saleMTPumps' },
    { row: 89, property: 'saleBTPMEIPrivate' },
    { row: 90, property: 'saleBTPMEIAdministration' },
    { row: 91, property: 'saleBTInternal' },
    { row: 92, property: 'saleBTPumps' },
    { row: 94, property: 'saleBTResPrivate' },
    { row: 95, property: 'saleBTResAdministration' },
    { row: 96, property: 'saleBTResAgent' },
    { row: 97, property: 'saleLighting' },
    { row: 103, property: 'connectionsMTPrivate' },
    { row: 104, property: 'connectionsMTAdministration' },
    { row: 105, property: 'connectionsMTPumps' },
    { row: 108, property: 'connectionsBTPMEIPrivate' },
    { row: 109, property: 'connectionsBTPMEIAdministration' },
    { row: 110, property: 'connectionsBTPMEIInternal' },
    { row: 111, property: 'connectionsBTPMEIPumps' },
    { row: 113, property: 'connectionsBTResPrivate' },
    { row: 114, property: 'connectionsBTResAdministration' },
    { row: 115, property: 'connectionsBTResAgent' },
    { row: 116, property: 'connectionsLighting' },
    { row: 121, property: 'newConnectionsMT' },
    { row: 124, property: 'newConnectionQuotationsBT' },
    { row: 125, property: 'newConnectionsOrderedBT' },
    { row: 126, property: 'newConnectionsBT' },
    { row: 127, property: 'newConnectionsPaidAndPending' },
    { row: 133, property: 'cutsMT' },
    { row: 134, property: 'cutsMTHours' },
    { row: 136, property: 'cutsBT' },
    { row: 174, property: 'avgPriceMT' },
    { row: 175, property: 'avgPriceMTPrivate' },
    { row: 176, property: 'avgPriceMTAdministration' },
    { row: 177, property: 'avgPriceMTPumps' },
    { row: 178, property: 'avgPriceBT' },
    { row: 179, property: 'avgPriceBTPMEI' },
    { row: 180, property: 'avgPriceBTPMEIPrivate' },
    { row: 181, property: 'avgPriceBTPMEIAdministration' },
    { row: 182, property: 'avgPriceBTPMEIInternal' },
    { row: 183, property: 'avgPriceBTPMEIPumps' },
    { row: 184, property: 'avgPriceBTRes' },
    { row: 185, property: 'avgPriceBTResPrivate' },
    { row: 186, property: 'avgPriceBTResAdministration' },
    { row: 187, property: 'avgPriceBTResAgent' },
    { row: 188, property: 'avgPriceLighting' }
];

module.exports = {
    FILE_STRUCTURE
};
