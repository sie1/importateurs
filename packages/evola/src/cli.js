const { createReadStream, createWriteStream } = require('fs');
const { EventEmitter } = require('events');
const { noop } = require('lodash/fp');
const yargs = require('yargs');
const { transform } = require('./transform');

module.exports = {
    async runCli() {
        preventNodeExit();

        const argv = yargs
            .alias('t', 'transformation')
            .default('transformation', 'psql')
            .argv;

        const { src, dest } = getSrcAndDest(yargs.argv._);
        await transform(src, dest, argv);

        allowNodeExit();
    }
};

const eventEmitter = new EventEmitter();

function preventNodeExit() {
    eventEmitter.emit('block', { intention: 'block' });
}

function allowNodeExit() {
    eventEmitter.on('block', noop);
}

function getSrcAndDest(args) {
    const srcArg = yargs.argv._[0];
    const src = createReadStream(srcArg);
    const destArg = yargs.argv._[1];
    const dest = !destArg
        ? destFromSource(srcArg)
        : destArg === '-'
        ? process.stdout
        : createWriteStream(destArg);

    return { src, dest };
}

/**
 *
 * @param {string} src
 */
function destFromSource(src) {
    const base = /^(.*\/)?([^\\\/]*?)(\.[^.\\\/]*)?$/.exec(src);
    return createWriteStream(`${base[1]}${base[2]}.sql`);
}
