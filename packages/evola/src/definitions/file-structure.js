/**
* @typedef {Object} DataField
* @property {number} row
* @property {string} property
*/


/**
 * @type {DataField[]}
 */
const FILE_STRUCTURE = [
   { row: 9, property: 'year' },
   { row: 10, property: 'grossProduction' },
   { row: 11, property: 'hydro' },
   { row: 15, property: 'thermalGO' },
   { row: 19, property: 'thermalFO' },
   { row: 23, property: 'thermalSolar' },
   { row: 25, property: 'netProduction' },
   { row: 26, property: 'energyDelivery' },
   { row: 30, property: 'consumptionGO' },
   { row: 33, property: 'consumptionFO' },
   { row: 40, property: 'piHydro' },
   { row: 44, property: 'piThermalGO' },
   { row: 48, property: 'piThermalFO' },
   { row: 52, property: 'piThermalSolar' },
   { row: 55, property: 'pdHydro' },
   { row: 59, property: 'pdThermalGO' },
   { row: 63, property: 'pdThermalFO' },
   { row: 67, property: 'pdThermalSolar' },
   { row: 70, property: 'maxProduction' },
   { row: 76, property: 'saleMTPrivate' },
   { row: 77, property: 'saleMTAdministration' },
   { row: 78, property: 'saleMTPumps' },
   { row: 81, property: 'saleBTPMEIPrivate' },
   { row: 82, property: 'saleBTPMEIAdministration' },
   { row: 83, property: 'saleBTInternal' },
   { row: 84, property: 'saleBTPumps' },
   { row: 86, property: 'saleBTResPrivate' },
   { row: 87, property: 'saleBTResAdministration' },
   { row: 88, property: 'saleBTResAgent' },
   { row: 89, property: 'saleLighting' },
   { row: 95, property: 'connectionsMTPrivate' },
   { row: 96, property: 'connectionsMTAdministration' },
   { row: 97, property: 'connectionsMTPumps' },
   { row: 100, property: 'connectionsBTPMEIPrivate' },
   { row: 101, property: 'connectionsBTPMEIAdministration' },
   { row: 102, property: 'connectionsBTPMEIInternal' },
   { row: 103, property: 'connectionsBTPMEIPumps' },
   { row: 105, property: 'connectionsBTResPrivate' },
   { row: 106, property: 'connectionsBTResAdministration' },
   { row: 107, property: 'connectionsBTResAgent' },
   { row: 108, property: 'connectionsLighting' },
   { row: 114, property: 'revenueMTPrivate' },
   { row: 115, property: 'revenueMTAdministration' },
   { row: 116, property: 'revenueMTPumps' },
   { row: 119, property: 'revenueBTPMEIPrivate' },
   { row: 120, property: 'revenueBTPMEIAdministration' },
   { row: 121, property: 'revenueBTPMEIInternal' },
   { row: 122, property: 'revenueBTPumps' },
   { row: 124, property: 'revenueBTResPrivate' },
   { row: 125, property: 'revenueBTResAdministration' },
   { row: 126, property: 'revenueBTResAgent' },
   { row: 127, property: 'revenueLighting' },
   { row: 132, property: 'newConnectionsMT' },
   { row: 135, property: 'newConnectionQuotationsBT' },
   { row: 136, property: 'newConnectionsOrderedBT' },
   { row: 137, property: 'newConnectionsBT' },
   { row: 138, property: 'newConnectionsPaidAndPending' },
   { row: 144, property: 'cutsMT' },
   { row: 145, property: 'cutsMTHours' },
   { row: 147, property: 'cutsBT' }
];

module.exports = {
    FILE_STRUCTURE
};
