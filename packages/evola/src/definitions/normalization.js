const { nonNet } = require('./normalization-conditions');
const { def, div, fac, flow, monthly } = require('./normalization-modifications');

const NORMALIZATIONS = [
    { code: 'EL01', field: 'grossProduction', modification: monthly() },
    { code: 'EL015HY', field: 'hydro', modification: monthly() },
    { code: 'EL015CG', field: 'thermalGO', modification: monthly() },
    { code: 'EL015CF', field: 'thermalFO', modification: monthly() },
    { code: 'EL015ST', field: 'thermalSolar', modification: monthly() },
    { code: 'EL015HB', field: 'hybrid', modification: monthly() },
    { code: 'EL019', field: 'netProduction', modification: monthly() },
    { code: 'EC133D', field: 'energyDelivery', modification: monthly() },
    { code: 'DL08811', field: 'consumptionGO', modification: flow(fac(1000), monthly()) },
    { code: 'RF08811', field: 'consumptionFO', modification: flow(fac(1000), monthly()) },
    { code: 'EC13321', field: 'piHydro' },
    { code: 'EC13341EG', field: 'piThermalGO' },
    { code: 'EC13341EF', field: 'piThermalFO' },
    { code: 'EC13361', field: 'piThermalSolar' },
    { code: 'EC13328', field: 'pdHydro' },
    { code: 'EC13348EG', field: 'pdThermalGO' },
    { code: 'EC13348EF', field: 'pdThermalFO' },
    { code: 'EC13368', field: 'pdThermalSolar' },
    { code: 'EC13390', field: 'maxProduction' },
    { code: 'EL121M', field: 'saleMTPrivate', modification: monthly(), conditional: nonNet() },
    { code: 'EL1235M', field: 'saleMTAdministration', modification: monthly(), conditional: nonNet() },
    { code: 'EL1214pM', field: 'saleMTPumps', modification: monthly(), conditional: nonNet() },
    { code: 'EL1235', field: 'saleBTPMEIPrivate', modification: monthly(), conditional: nonNet() },
    { code: 'EL1235A', field: 'saleBTPMEIAdministration', modification: monthly(), conditional: nonNet() },
    { code: 'EL1234I', field: 'saleBTInternal', modification: monthly(), conditional: nonNet() },
    { code: 'EL1214pB', field: 'saleBTPumps', modification: monthly(), conditional: nonNet() },
    { code: 'EL1231', field: 'saleBTResPrivate', modification: monthly(), conditional: nonNet() },
    { code: 'EL1231A', field: 'saleBTResAdministration', modification: monthly(), conditional: nonNet() },
    { code: 'EL1231J', field: 'saleBTResAgent', modification: monthly(), conditional: nonNet() },
    { code: 'EL1221L', field: 'saleLighting', modification: monthly(), conditional: nonNet() },
    { code: 'CO01', field: ['connectionsMTPrivate', 'connectionsMTAdministration', 'connectionsMTPumps'], modification: def(0), conditional: nonNet() },
    { code: 'CO0201', field: ['connectionsBTPMEIPrivate', 'connectionsBTPMEIAdministration', 'connectionsBTPMEIInternal', 'connectionsBTPMEIPumps'], modification: def(0), conditional: nonNet() },
    { code: 'CO0202', field: ['connectionsBTResPrivate', 'connectionsBTResAdministration', 'connectionsBTResAgent'], modification: def(0), conditional: nonNet() },
    { code: 'CN01', field: 'newConnectionsMT', modification: monthly(), conditional: nonNet() },
    { code: 'CN02', field: 'newConnectionsBT', modification: monthly(), conditional: nonNet() },
    { code: 'CUT01', field: 'cutsMT', modification: monthly() },
    { code: 'CUT01H', field: 'cutsMTHours', modification: monthly() },
    { code: 'CUT02', field: 'cutsBT', modification: monthly() },
    { code: 'EP01',   field: [['revenueMTPrivate', 'revenueMTAdministration', 'revenueMTPumps'], ['saleMTPrivate', 'saleMTAdministration', 'saleMTPumps']], modification: flow(div(), fac(1000)) },
    { code: 'EP0201', field: [['revenueBTPMEIPrivate', 'revenueBTPMEIAdministration'], ['saleBTPMEIPrivate', 'saleBTPMEIAdministration']], modification: flow(div(), fac(1000)) },
    { code: 'EP0202', field: [['revenueBTResPrivate', 'revenueBTResAdministration', 'revenueBTResAgent'], ['saleBTResPrivate', 'saleBTResAdministration', 'saleBTResAgent']], modification: flow(div(), fac(1000)) }
];

module.exports = {
    NORMALIZATIONS
};
