const { identity, invert, stubTrue } = require('lodash/fp');
const moment = require('moment');
const { v1: uuidV1 } = require('uuid');
const { NORMALIZATIONS } = require('./definitions/normalization');
const { winston } = require('./winston');

/**
 * @typedef {Object} NormalizedData
 * @property {string} importId
 * @property {(string[] | [string, string, string, string, string, string, string, number])[]} rows
 */

const JIRAMA_COMPANY_UUID = '11556ea8-3b66-11e9-ae4e-0242113dc113';

/**
 *
 * @param {any[][]} table
 * @returns {NormalizedData}
 */
function normalize(table) {
    winston.info('Normalizing data…');

    const rows = [['id', 'mois', 'id_societe', 'centrale', 'timestamp', 'import', 'code', 'valeur']];
    const importId = uuidV1();
    const normalized = table.slice(1).reduce(addTableRowToNormalizedMatrix(importId, table[0]), rows);

    return { importId, rows: normalized };
}

function addTableRowToNormalizedMatrix(importId, colNames) {
    const colIds = invert(colNames);
    const timestamp = moment().format();

    return function(normRows, row) {
        function get(col, mod) {
            let value;

            if (typeof col === 'string') {
                const raw = row[colIds[col]];
                value = typeof raw === 'undefined'
                    ? undefined
                    : raw === null || raw === ''
                    ? null
                    : !isNaN(+raw)
                    ? +raw
                    : typeof raw === 'string'
                    ? raw
                    : null;

            } else if (Array.isArray(col)) {
                value = col.map(subCol => get(subCol, identity));

                if (!col.length) {
                    value = Number.NaN;
                } else if (!Array.isArray(col[0])) {
                    value = value.reduce((sum, part) => sum + part, 0);
                }
            } else {
                value = Number.NaN;
            }

            return (mod || identity)(value);
        }

        const rowBaseDimensions = [
            get('year'),
            JIRAMA_COMPANY_UUID,
            get('areaId'),
            timestamp,
            importId
        ];

        const addNormRow = normRowBuilderFactory(rowBaseDimensions);
        NORMALIZATIONS
            .filter(normalization => (normalization.conditional || stubTrue)(rowBaseDimensions))
            .forEach(normalization => addNormRow(normRows, normalization.code, get(normalization.field, normalization.mmodification)));

        return normRows;
    };
}

function normRowBuilderFactory(baseDimensions) {
    const year = baseDimensions[0];
    const noDate = baseDimensions.slice(1);

    return function addCopyRow(rows, code, value) {
        if (value === null || value === undefined) {
            return;
        }

        for (let month = 0; month < 12; month++) {
            rows.push([uuidV1(), moment([year, month, 1]).format('YYYY-MM-DD'), ...noDate, code, value]);
        }
    }
}

module.exports = {
    normalize
};
