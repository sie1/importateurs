if (process.mainModule === module) {
    require('./cli').cli();
} else {
    module.exports = {
        transformShp: require('./cli').transformShp
    };
}
