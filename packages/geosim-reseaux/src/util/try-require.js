const { join } = require('path');

module.exports = {
    tryLoadModule(dir, name) {
        try {
            const fullPath = join(dir, name);
            return require(fullPath);
        } catch (e) {
            return undefined;
        }
    }
}
