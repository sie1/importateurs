const { EventEmitter } = require('events');
const { createWriteStream } = require('fs');
const { noop } = require('lodash/fp');
const yargs = require('yargs');
const { readShp } = require('./read-shp');
const { transform } = require('./transform');
// @ts-ignore
const defaultOptions = require('./default-options');

module.exports = {
    async cli() {
        preventNodeExit();

        const options = getOptions();

        const src = options.source;

        const transformedResult = await module.exports.transformShp(src, options);

        const output = options.output === '-'
            ? process.stdout
            : createWriteStream(options.output);

        return new Promise((resolve, reject) => {
            output.on('finish', resolve);
            output.on('error', reject);

            // @ts-ignore
            output.write(transformedResult);
            if (output !== process.stdout) {
                output.end();
            }

            allowNodeExit();
        });
    },

    async transformShp(src, options) {
        const result = await readShp(src, options.dbf || undefined);
        return await transform(result, options);
    }
}

function getOptions() {
    const options = yargs
        .alias('t', 'transformation')
        .alias('g', 'geotransform')
        .alias('s', 'source')
        .alias('o', 'output')
        .alias('d', 'dbf')
        .default(defaultOptions)
        .argv;

    if (!options.source) {
        options.source = options._[0];
    }

    return options;
}

const eventEmitter = new EventEmitter();

function preventNodeExit() {
    eventEmitter.emit('block', { intention: 'block' });
}

function allowNodeExit() {
    eventEmitter.on('block', noop);
}
