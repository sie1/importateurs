const { reseauMapperFactory } = require("./reseau-mapper-factory");

/**
 * @type {Record<string, string | [string, (val: string) => any]>} */
const columnMappingReseauHt = {
    TensionkV: ['tension', val => +val * 1000],
    ID: 'id_reseau',
    Longueur: 'longueur'
};

module.exports = reseauMapperFactory('reseauHt', columnMappingReseauHt)
    .patch(function (downStream, data) {
        const result = downStream(data);

        return result.map(feature => {
            feature.type = 1;
            feature.existant = true;
            return feature;
        });
    });
