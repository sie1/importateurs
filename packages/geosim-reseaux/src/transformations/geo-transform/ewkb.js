const wkx = require('wkx');

module.exports = {
    transform(data) {
        return wkx.Geometry
            .parseGeoJSON(data)
            .toEwkb()
            .toString('hex');
    }
};
