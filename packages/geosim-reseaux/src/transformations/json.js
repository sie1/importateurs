const { join } = require('path');
const { tryLoadModule } = require('../util/try-require');

/**
 *
 * @param {import('../read-shp').GeoFeature[]} data
 * @param {Record<string, string>} options
 */
async function transformation(data, options) {
    const geotransform = loadGeoTransform(options.geotransform);
    const transformed = geotransform
        ? data.map(feature => ({ ...feature, the_geom: geotransform(feature.the_geom) }))
        : data;

    return JSON.stringify(transformed);
}

/**
 *
 * @param {string=} transformation
 * @returns {((geodata: import('geojson').Geometry) => any) | undefined}
 */
function loadGeoTransform(transformation) {
    if (!transformation) {
        return undefined;
    }

    const geotransformDir = join(__dirname, 'geo-transform');
    const geotransform = tryLoadModule(geotransformDir, transformation);
    return geotransform && geotransform.transform;
}

module.exports = {
    transformation
};
