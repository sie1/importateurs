const { identity, invert } = require('lodash/fp');
const moment = require('moment');
const { v1: uuidV1 } = require('uuid');
const { NORMALIZATIONS } = require('./definitions/normalization');
const { winston } = require('./winston');

/**
 * @typedef {Object} NormalizedData
 * @property {string} importId
 * @property {(string[] | [string, string, string, string, string, string, string, number])[]} rows
 */

/**
 *
 * @param {any[][]} table
 * @returns {NormalizedData}
 */
function normalize(table) {
    winston.info('Normalizing data…');

    const rows = [['id', 'mois', 'societe', 'centrale', 'timestamp', 'import', 'code', 'valeur']];
    const importId = uuidV1();
    const normalized = table.slice(1).reduce(addTableRowToNormalizedMatrix(importId, table[0]), rows);

    return { importId, rows: normalized };
}

function def(defVal) {
    return function(val) {
        return isNaN(+val) ? defVal : val;
    }
}

function addTableRowToNormalizedMatrix(importId, colNames) {
    const colIds = invert(colNames);
    const timestamp = moment().format();

    return function(normRows, row) {
        function get(col, mod) {
            if (Array.isArray(col)) {
                return (mod || identity)(col.reduce((acc, subCol) => acc + get(subCol), 0));
            }

            const raw = row[colIds[col]];
            const normalized = typeof raw === 'undefined'
                ? undefined
                : raw === null || raw === ''
                ? null
                : !isNaN(+raw)
                ? +raw
                : typeof raw === 'string'
                ? raw
                : null;

            return (mod || identity)(normalized);
        }

        const rowBaseDimensions = [
            get('year'),
            get('societe'),
            get('siteName'),
            timestamp,
            importId
        ];

        const addNormRow = normRowBuilderFactory(rowBaseDimensions);
        NORMALIZATIONS.forEach(normalization => addNormRow(normRows, normalization.code, get(normalization.field, normalization.modification)));

        return normRows;
    };
}

function normRowBuilderFactory(baseDimensions) {
    const year = baseDimensions[0];
    const noDate = baseDimensions.slice(1);

    return function addCopyRow(rows, code, value) {
        if (value === null || value === undefined) {
            return;
        }

        for (let month = 0; month < 12; month++) {
            rows.push([uuidV1(), moment([year, month, 1]).format('YYYY-MM-DD'), ...noDate, code, value]);
        }
    }
}

module.exports = {
    normalize
};
