const { basename } = require('path');
const { Readable } = require('stream');
const { winston } = require('../winston');

const IMPORT_TYPE = 'ADER_MATRICE';

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, options) {

        return new PsqlScriptStream(data, options);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, options) {
        super();

        this.filename = basename(options._[0]);
        this.rows= data.rows;
        this.importId = data.importId;
        this.options = options;
        this.iter = this._readLine();
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {
        const cols = this.rows[0];
        yield `begin transaction;
delete from etl_matrice m using imports i where m.import = i.id and i.filename = '${this.filename}';
insert into imports(id, type, "timestamp", filename) values ('${this.importId}', '${IMPORT_TYPE}', NOW(), '${this.filename}');
insert into etl_matrice(${cols.map(col => col === 'societe' ? 'id_societe' : col).join(', ')}) values\n`;

        winston.info('Converting data into PSQL copy data…');

        for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
            const sep = rowIndex > 1 ? ',\n' : '';
            const rowData = this.rows[rowIndex];
            const insertValues = buildInsertValues(cols, rowData);
            this.rows[rowIndex] = null;
            yield `${sep} (${insertValues.join(',')})`;
        }

        yield ';\n\ncommit transaction;\n';
    }
}

/**
 *
 * @param {string[]} cols
 * @param {unknown[]} data
 */
function buildInsertValues(cols, data) {
    return cols
        .map((col, colIndex) => {
            const val = data[colIndex];
            if (col === 'societe') {
                return `(select id from company where name = '${('' + val).replace(/'/g, '\'\'')}')`;
            } else if (typeof val === 'string') {
                return `'${val.replace(/'/g, '\'\'')}'`;
            } else {
                return `${val}`;
            }
        });
}
