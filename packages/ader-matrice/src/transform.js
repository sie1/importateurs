const { ReadStream } = require('fs');
const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const { v1: uuidV1 } = require('uuid');
const { winston } = require('./winston');
const { read } = require('xlsx');
const { FILE_STRUCTURE } = require('./definitions/file-structure');
const { num } = require('./definitions/micro-transform');
const { normalize } = require('./normalize');
const { compareCols, incCol } = require('./sheet-utils');
const { streamToBuffer } = require('./stream-to-buffer');

/** @typedef {Object} Company
 * @property {string} name
 * @property {string} start
 * @property {string} end
 * @property {Record<string, unknown>[]} sites
 */

module.exports = {
    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {Record<string, unknown>} options
     */
    async transform(src, dest, options) {
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            winston.info(`Reading source file "${src.path}"…`);
        } else {
            winston.info('Reading source stream…');
        }

        let dataSheet = await prepareDataSheet(src);
        winston.info(`Preparing input data from Excel sheet…`);
        const year = +/\d+/.exec(dataSheet['A1'].v)[0]

        const companies = readCompanyList(dataSheet);
        companies.forEach(comp => readSites(dataSheet, comp));
        let tables = buildTable(companies, year);
        const normalized = normalize(tables);

        dataSheet = tables = null;
        return output(normalized, dest, transformation, options);
    }
};

/**
 *
 * @param {Readable} src
 */
async function prepareDataSheet(src) {
    const srcBuf = await streamToBuffer(src);
    const book = read(srcBuf);
    const statSheet = book.SheetNames.filter(name => /stat expl/i.test(name));
    if (!statSheet.length) {
        throw new Error('Data sheet not found');
    } else if (statSheet.length > 1) {
        throw new Error(`More than one candidate sheet found (${statSheet.join(', ')})`);
    }

    return book.Sheets[statSheet[0]];
}

/**
 *
 * @param {import('./normalize').NormalizedData} data
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function output(data, dest, transformation, options) {
    await new Promise((resolve, reject) => {
        const outData = loadTransformation(transformation).default(data, options);

        if (isStream.readable(outData)) {
            outData.pipe(dest);
            dest.on('finish', () => resolve());
            outData.on('error', reject);
        } else {
            dest.write(
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Failed to load transformation ${transformation}`);
    }
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @returns {Company[]}
 */
function readCompanyList(sheet) {
    const companies = [];

    const finalCol = /:([A-Z]+)/.exec(sheet['!ref'])[1];
    for (let col = 'B'; compareCols(col, finalCol) <= 0; col = incCol(col)) {
        const cell = sheet[`${col}3`];
        if (cell) {
            companies.push({ name: cell.v.trim(), start: col, end: col, sites: [] });
        } else if (companies.length) {
            companies[companies.length - 1].end = col;
        }
    }

    return companies;
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {Company} company
 * @returns {Company}
 */
function readSites(sheet, company) {
    for (let col = company.start; compareCols(col, company.end) <= 0; col = incCol(col)) {
        company.sites.push(readSite(sheet, col));
    }

    fixSiteNames(company.sites);

    return company;
}

/**
 *
 * @param {Record<string, unknown>[]} sites
 */
function fixSiteNames(sites) {
    const generatedNames = new Set();
    for (let site of sites) {
        if (site.siteName) {
            continue;
        }

        const communities = site.communities;
        let indexedCommunities;
        let index = 0;
        do {
            index++;
            const romanIndex = romanNumeral(index);
            indexedCommunities = `${communities} ${romanIndex}`;
        } while(generatedNames.has(indexedCommunities));

        site.siteName = indexedCommunities;
        generatedNames.add(indexedCommunities);
    }
}

const romanSegments = [
    ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'],
    ['X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'],
    ['C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'],
    ['M', 'MM', 'MMM']
];
function romanNumeral(num, power = 0, suffix = '') {
    const decimal = num % 10;
    const segment = decimal ? romanSegments[power][decimal - 1] : '';
    const nextSuffix = `${segment}${suffix}`;
    const nextNum = (num / 10) | 0;
    if (!nextNum) {
        return nextSuffix;
    }

    return romanNumeral(nextNum, power + 1, nextSuffix);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} col
 * @returns {Record<string, unknown>}
 */
function readSite(sheet, col) {
    const site = {};

    return FILE_STRUCTURE.reduce(assignField(sheet, col), site);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} col
 * @returns {(site: Record<string, unknown>, field: import('./definitions/file-structure').DataField) => Record<string, unknown>}
 */
function assignField(sheet, col) {
    return function (site, field) {
        const cell = `${col}${field.row}`;
        const raw = sheet[cell] && sheet[cell].v;
        const defaultTransform = num();
        const transformations = Array.isArray(field.transform)
            ? field.transform
            : [field.transform || defaultTransform];
        site[field.property] = transformations
            .reduce(
                (intermediate, fn) => fn(intermediate),
                raw === null || raw === undefined ? '' : raw
            );

        return site;
    };
}


function buildTable(companies, year) {
    return companies.reduce(
        addCompanyToTableFor(year),
        getEmptyTable()
    );
}

/**
 * @returns {[string[], ...unknown[][]]}
 */
function getEmptyTable() {
    return [[
        'id',
        'year',
        'societe',
        ...FILE_STRUCTURE.map(field => field.property)
    ]];
}

/**
 *
 * @param {number} year
 * @returns {(table: [string[], ...unknown[][]], company: Company) => [string[], ...unknown[][]]}
 */
function addCompanyToTableFor(year) {
    /**
     * @param {unknown[][]} table
     * @param {Company} company
     */
    return function addCompanyToTables(table, company) {
        company.sites.forEach(site => {
            const siteRecordId = uuidV1();
            const productionRecord = [
                siteRecordId,
                year,
                company.name,
                ...table[0].slice(3).map(prop => site[prop])
            ];

            table.push(productionRecord);
        });

        return table;
    };
}
