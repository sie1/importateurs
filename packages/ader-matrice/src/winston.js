const winston = require('winston');

module.exports = {
    winston: winston.createLogger({
        level: 'info',
        format: winston.format.simple(),
        transports: [
            new winston.transports.Console({
                stderrLevels: ['silly', 'debug', 'verbose', 'info', 'warn', 'error'],
                format: winston.format.colorize({ all: true,  })
            })
        ]
    })
}
