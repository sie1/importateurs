const { str } = require('./micro-transform');

/**
 * @typedef {Object} DataField
 * @property {number} row
 * @property {string} property
 * @property {Function | Function[]=} transform
 */

 /**
  * @type {DataField[]}
  */
 const FILE_STRUCTURE = [
    { row: 11, property: 'siteName', transform: str() },
    { row: 6, property: 'province', transform: str() },
    { row: 7, property: 'region', transform: str() },
    { row: 8, property: 'district', transform: str() },
    { row: 9, property: 'communities', transform: inp => `${inp}`.split(/[-,]/g).map(com => com.trim()).join(';') },
    { row: 30, property: 'population' },
    { row: 31, property: 'households' },
    { row: 35, property: 'hydroInstalled' },
    { row: 36, property: 'hydroAvailable' },
    { row: 38, property: 'thermalInstalled' },
    { row: 39, property: 'thermalAvailable' },
    { row: 41, property: 'biomatterInstalled' },
    { row: 42, property: 'biomatterAvailable' },
    { row: 44, property: 'solarInstalled' },
    { row: 45, property: 'solarAvailable' },
    { row: 48, property: 'windInstalled' },
    { row: 49, property: 'windAvailable' },
    { row: 114, property: 'subsriptionsPopulation' },
    { row: 115, property: 'subscriptionsHouseholds' },
    { row: 116, property: 'subscriptionsCommercial' },
    { row: 117, property: 'subscriptionsAdministration' },
    { row: 118, property: 'subscriptionsPublicLighting' },
    { row: 121, property: 'productionHydro' },
    { row: 122, property: 'productionThermal' },
    { row: 123, property: 'productionBiomatter' },
    { row: 124, property: 'productionSolar' },
    { row: 126, property: 'maxProduction' },
    { row: 128, property: 'totalConsumption' }
];

module.exports = {
    FILE_STRUCTURE
};
