/** Convertir en forme numérique */
function num() {
    return function (inp) {
        const n = +inp;

        return isNaN(n) ? null : n;
    }
}

/** Convertir en texte */
function str() {
    return function (inp) {
        return `${inp}`.trim();
    }
}

module.exports = {
    num,
    str
};
