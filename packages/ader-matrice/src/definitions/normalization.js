const { fac, flow, monthly } = require('./normalization-modifications');

/**
 * @typedef {Object} Normalization
 * @property {string} code
 * @property {boolean=} divide
 * @property {string | string[]} field
 * @property {Function=} modification
 */

/**
 * @type {Normalization[]}
 */
const NORMALIZATIONS = [
    { code: 'EL01', field: ['productionHydro', 'productionThermal', 'productionBiomatter', 'productionSolar'], modification: flow(fac(.001), monthly()) },
    { code: 'EL015HY', field: 'productionHydro', modification: flow(fac(.001), monthly()) },
    { code: 'EL015CG', field: 'productionThermal', modification: flow(fac(.001), monthly()) },
    { code: 'EL015ST', field: 'productionSolar', modification: flow(fac(.001), monthly()) },
    { code: 'EL015W', field: 'productionWind', modification: flow(fac(.001), monthly()) },
    { code: 'EL015BM', field: 'productionBiomatter', modification: flow(fac(.001), monthly()) },
    { code: 'EC13321' , field: 'hydroInstalled' },
    { code: 'EC13341EG' , field: 'thermalInstalled' },
    { code: 'EC13361' , field: 'solarInstalled' },
    { code: 'EC13391BM' , field: 'biomatterInstalled' },
    { code: 'EC13351' , field: 'windInstalled' },
    { code: 'EC13328' , field: 'hydroAvailable' },
    { code: 'EC13348EG' , field: 'thermalAvailable' },
    { code: 'EC13368' , field: 'solarAvailable' },
    { code: 'EC13398BM' , field: 'biomatterAvailable' },
    { code: 'EC13358' , field: 'windAvailable' },
    { code: 'EC13390' , field: 'maxProduction' },
    { code: 'CO0201' , field: ['subscriptionsCommercial', 'subscriptionsAdministration'] },
    { code: 'CO0202' , field: 'subscriptionsHouseholds' },
    { code: 'CO03' , field: 'subscriptionsPublicLighting' },
    { code: 'EL1231', field: 'totalConsumption', modification: monthly() }
];

module.exports = {
    NORMALIZATIONS
};
